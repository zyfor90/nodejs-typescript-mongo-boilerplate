import { injectable, inject } from 'inversify';
import { Controller } from '../app/symbols';
import TestingController from '../app/testing/testing.controller';
import asyncWrap from '../middlewares/async.wrapper.middleware';
import BaseController from '../common/base.controller';


@injectable()
export default class Router {

    // * Define all injection here.
    // * if you want to create a new service, you have to register your services inversify.ts
    // * please check src/app/sysmbol.ts and src/app/inversify.ts

    @inject(Controller.TestingController) public testingController: TestingController;

    // ! We need to bind proper context to the controller methods
    public getController(context: BaseController, func: string) {
        return asyncWrap(context[func].bind(context));
    }
}