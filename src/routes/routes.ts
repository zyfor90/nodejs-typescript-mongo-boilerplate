import { Application } from 'express';
import Router from './router';

export default class ApplicationRouter extends Router {

  // * Register your routes here
  // * Note: before you create any routes here, you must inject your controller in router.ts

  public register(app: Application) {
    app.get('/testing', this.getController(this.testingController, 'Testing'));
  }

}
