declare namespace Express {
    export interface Response {
        SuccessResponse(message: string, statusCode: number, data: any): void
        ErrorResponse(message: string, statusCode: number): void
        InternalServerErrorResponse(): void
    }
}