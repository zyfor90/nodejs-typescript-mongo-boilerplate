import { Request, Response, NextFunction } from 'express'

interface Responser {
    meta: Meta;
    data: any;
}

interface Meta {
    code: number;
    message: string;
    status: string;
}

export function extendedResponse(req: Request, res: Response, next: NextFunction) {
    res.SuccessResponse = function (message, statusCode, data) {
        const res: Responser = {
            meta: {
                code: statusCode,
                message: message,
                status: "success",
            },
            data: data,
        };

        this.status(statusCode).json(res);
    }

    res.ErrorResponse = function (message, statusCode) {
        const res: Responser = {
            meta: {
                code: statusCode,
                message: message,
                status: "error",
            },
            data: [],
        };

        this.status(statusCode).json(res);
    }

    res.InternalServerErrorResponse = function () {
        const res: Responser = {
            meta: {
                code: 500,
                message: "Internal Server Error",
                status: "error",
            },
            data: [],
        };

        this.status(500).json(res);
    }

    next()
}