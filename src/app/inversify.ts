import { Container } from 'inversify';
import { Controller, Repository, Service } from './symbols';
import TestingController from './testing/testing.controller';
import ApplicationRouter from '../routes/routes';
import TestingService from './testing/testing.service';
import TestingRepostiory from './testing/testing.repository';

const container = new Container({ defaultScope: 'Singleton' });

// Like other dependencies we do not resolve ApplicationRouter via `TYPES`.
// We get the instance of the class only in app.ts file during bootstrap.

container.bind(ApplicationRouter).to(ApplicationRouter);

// * Testing Services
container.bind<TestingController>(Controller.TestingController).to(TestingController);
container.bind<TestingService>(Service.TestingService).to(TestingService);
container.bind<TestingRepostiory>(Repository.TestingRepository).to(TestingRepostiory);

// * User Services
// container.bind<TestingController>(Controller.TestingController).to(TestingController);
// container.bind<TestingService>(Service.TestingService).to(TestingService);
// container.bind<TestingRepostiory>(Repostiory.TestingRepostiory).to(TestingRepostiory);

export default container;
