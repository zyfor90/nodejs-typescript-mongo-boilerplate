import { injectable, inject } from 'inversify';
import { Request, Response } from 'express';
import { Service } from '../symbols';
import BaseController from '../../common/base.controller';
import ITestingService from './testing.service';

@injectable()
export default class TestingController extends BaseController {
    @inject(Service.TestingService) private testingService: ITestingService;

    public async Testing(_req: Request, res: Response): Promise<void> {
        const testing = await this.testingService.getAll();

        res.SuccessResponse("Berhasil membuka controller testing", 200, testing);
    }
}
