import { injectable, inject } from 'inversify';
import { ITesting, testingEntities } from '../../domain/entities/testing.entities';

interface ITestingRepostiory {
    getAll(): Promise<ITesting[]>;
}

@injectable()
export default class TestingRepostiory implements ITestingRepostiory {
    public async getAll(): Promise<ITesting[]> {
        return await testingEntities.find({});
    }
}
