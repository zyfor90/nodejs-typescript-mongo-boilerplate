import { injectable, inject } from 'inversify';
import { Repository } from '../symbols';
import { ITesting } from '../../domain/entities/testing.entities';
import ITestingRepostiory from './testing.repository';

interface ITestingService {
    getAll(): Promise<ITesting[]>;
}

@injectable()
export default class TestingService implements ITestingService {
    @inject(Repository.TestingRepository) private testingRepository: ITestingRepostiory;
    public async getAll(): Promise<ITesting[]> {
        return await this.testingRepository.getAll()
    }
}
