/**
 * InversifyJS need to use the type as identifiers at runtime.
 * We use symbols as identifiers but you can also use classes and or string literals.
 */

export const Controller = {
    TestingController: Symbol('TestingController'),
};

export const Service = {
    TestingService: Symbol('TestingService'),
};

export const Repository = {
    TestingRepository: Symbol('TestingRepository'),
};