import mongoose, { Document, Model, Schema } from 'mongoose';

// Create a TypeScript interface to represent your document
export interface ITesting extends Document {
    _id: string;
    name: string;
}

const testingSchema = new Schema<ITesting>({
    _id: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
        unique: true,
    },
});

// Use TypeScript generics to define the model and document type
export const testingEntities: Model<ITesting> = mongoose.model('testing', testingSchema, 'testing');