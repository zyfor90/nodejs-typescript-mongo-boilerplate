import mongoose from 'mongoose';
import { EventEmitter } from 'events';
import logger from './logger';

/**
 * All the methods and properties mentioned in the following class is
 * specific to MongoDB. You should make necessary changes to support
 * the database/orm you want to use.
 */
class Database extends EventEmitter {
  private password: string;
  private user: string;
  private host: string;
  private dbName: string;
  private dbClient: typeof mongoose;
  private mongoProtocol = 'mongodb';

  constructor() {
    super();

    this.password = process.env.DATABASE_PASSWORD;
    this.user = process.env.DATABASE_USER;
    this.host = process.env.DATABASE_HOST;
    this.dbName = process.env.DATABASE_NAME;

    if (process.env.MONGO_PROTOCOL) {
      this.mongoProtocol = process.env.MONGO_PROTOCOL;
    }
  }

  public async connect(): Promise<void> {
    if (this.dbClient) {
      logger.debug('Connection already exists');
      return;
    }

    if (!this.password) {
      throw new Error('Database password not found');
    }

    if (!this.user) {
      throw new Error('Database user not found');
    }

    if (!this.host) {
      throw new Error('Database host not found');
    }

    if (!this.dbName) {
      throw new Error('Database name not found');
    }

    const connectionString = this.getConnectionString();

    logger.debug(`Database connection string: ${connectionString}`);

    try {
      this.dbClient = await mongoose.connect(connectionString, { dbName: this.dbName });
      logger.info('Connected with database host');
      this.emit('connected');
    } catch (e) {
      logger.error('Failed to connect to database', e.stack);
    }
  }

  public async disconnect() {
    if (this.dbClient) {
      logger.info(`Disconnected from ${this.host}`);
      await this.dbClient.disconnect();
    }
  }

  /**
   * Build database connection string.
   * Customize as needed for your database.
   */
  private getConnectionString() {
    return `${this.mongoProtocol}://${this.user}:${this.password}@${this.host}`;
  }

  public getHost() {
    return this.host;
  }

  public getPassword() {
    return this.password;
  }

  public getUser() {
    return this.user;
  }

  public getName() {
    return this.dbName;
  }

  public isConnected() {
    return Boolean(this.dbClient);
  }
}

export default new Database();
