import { Application, Request, Response, NextFunction } from 'express';
import { NotFoundError, ApplicationError } from '../common/app.errors';
import { MongoError } from 'mongodb';
import log from './logger';

export default function (app: Application) {
  // If you are lost
  app.use(() => {
    throw new NotFoundError('Not Found');
  });

  // Request error handler
  app.use(
    (
      error: ApplicationError,
      _req: Request,
      res: Response,
      next: NextFunction,
    ) => {
      if (error instanceof ApplicationError) {
        log.error(error?.message, error.stack);
        if (error.message) {
          return res.ErrorResponse(error.message, error.code);
        } else {
          return res.sendStatus(error.code);
        }
      }

      next(error);
    },
  );

  // Log all errors
  app.use(function (
    err: Error,
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    const userString = 'unknown user';

    if (err instanceof MongoError) {
      if (err.code === 11000) {
        log.error(
          `${req.method} ${req.path}: MongoDB duplicate entry from ${userString}`,
        );
      } else {
        log.error(
          `${req.method} ${req.path}: Unhandled MongoDB error ${userString}. ${err.errmsg}`,
        );
      }

      if (!res.headersSent) {
        return res.InternalServerErrorResponse();
      }
    } else if (err instanceof Error) {
      if (err.message == "Not Found") {
        return res.ErrorResponse("Not Found", 404);
      } else {
        log.error(
          `${req.method} ${req.path}: Unhandled request error ${userString}. ${err.message}`,
        );
        return res.InternalServerErrorResponse();
      }
    } else if (typeof err === 'string') {
      log.error(
        `${req.method} ${req.path}: Unhandled request1 error ${userString}. ${err}`,
      );
    }

    next(err);
  });

  // Optional fallthrough error handler
  app.use(function (
    err: Error,
    _req: Request,
    res: Response,
    _next: NextFunction,
  ) {
    res.statusCode = 500;
    res.end(err.message + '\n');
  });
}
