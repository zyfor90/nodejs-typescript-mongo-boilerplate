### NODEJS TYPESCRIPT MONGO BOILERPLATE

# Information about this boilerplate

- Author: Shihab Mridha
- Develop: Febriansyah

# Repository pattern & Dependency injection using TypeScript

This starter kit tries to implement a NodeJS, ExpressJS and MongoDB powered web application using repository pattern and dependency injection. The main idea is independent of any framework or database. TypeScript is used instead of JavaScript for various reasons. Especially the support for interface, generic type support, and better IntelliSense.

# Requirement

- Node Version : 20.9.0
- NPM Version : 10.1.0
- Mongodb

# Usage

- npm install -g pnpm
- pnpm run start:dev (hot relaod). You can check package json for available script.
- Go to `http://localhost:3000`. You should see a static html page.

# Core Features

- Dependency injection
- Repository pattern


# Dependency

- bcrypt: "5.1.0",
- compression: "1.7.4",
- cors: "2.8.5",
- dotenv: "16.1.4",
- express: "4.18.2",
- inversify: "6.0.1",
- mongoose: "^8.0.2",
- pnpm: "^8.11.0",
- reflecta": "0.1.13",
- saslprep: "^1.0.3",
- validator: "13.9.0",
- winston: "3.9.0"

## Dependency injection using InversifyJS

[InversifyJS](http://inversify.io/) is a very useful library for dependency injection in JavaScript. It has first class support for TypeScript. It is not necessary to use interface to use dependency injection because Inversify can work with class. But, we should **"depend upon Abstractions and do not depend upon concretions"**. So we will use interfaces (abstractions). Everywhere in our application, we will only use (import) interfaces. In the `src/core/inversify.ts` file we will create a container, import necessary classes and do dependency bindings. InversifyJS requires a library named [reflect-metadata](https://www.npmjs.com/package/reflect-metadata).

## Repository pattern

Main purpose is to decoupte database from business logic. If you ever decide to change the databse then you only need to update repositories. The base repository is `src/core/repository.ts`. All other repositores should be derived from it (Ex: `user.repository.ts`).
